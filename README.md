# [mb] cli client

This is the official command line client for mitarbeiterbereich application found at [app.mitarbeiterbereich.de](https://app.mitarbeiterbereich.de).

------

## Commands

### Configure application

First of all you have to configure your application.

    ./mb configure
    
Then you get ask for an api token. You can get one at the [API Tokens](http://app.mitarbeiterbereich.de/user/api-tokens) area in your settings at mitarbeiterbereich app.

### Tracking status

    ./mb track:status[ --date 2020-01-01]

You can receive your tracking for given date, today is default. Your configured token needs `tracking:time` for this command.


### Track work time

    ./mb track:time

You can start tracking your work time with this command. Your configured token needs `tracking:time` for this command.


### Track break time

    ./mb track:time --break

You can start tracking your break time with this command. Your configured token needs `tracking:time` for this command.


### Stop tracking time

    ./mb track:stop

You can stop tracking your time with this command. Your configured token needs `tracking:time` for this command.


### Self updating

    ./mb self-update

You can update the cli app by calling this command. It updates to the latest version remotely found.


## Support the development
**Do you like this project? Support it by donating**

- PayPal: [By me a coffee](https://paypal.me/rok)

## License

This client application is an open-source software licensed under the [MIT license](https://gitlab.com/mitarbeiterbereich/clients/mb-cli/-/blob/master/LICENSE).
