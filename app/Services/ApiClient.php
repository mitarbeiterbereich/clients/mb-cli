<?php

namespace App\Services;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class ApiClient
{
    private ?string $baseUrl = 'http://localhost/api';
    private ?string $token = null;
    private int $timeout = 3;

    public function __construct(string $baseUrl, ?string $token)
    {
        $this->baseUrl = $baseUrl;
        $this->token = $token;
    }

    public function withToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function withTimeout(int $timeout): self
    {
        $this->timeout = $timeout;

        return $this;
    }

    public function http(): PendingRequest
    {
        if (empty($this->token)) {
            throw new \RuntimeException('Api token is missing. Please configure the application.');
        }

        return Http::withToken($this->token)
            ->baseUrl($this->baseUrl)
            ->withoutRedirecting()
            ->timeout($this->timeout)
            ->retry(3, 10)
            ->acceptJson()
            ->asJson();
    }
}
