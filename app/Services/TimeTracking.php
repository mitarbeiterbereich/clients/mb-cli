<?php

namespace App\Services;

use Illuminate\Http\Client\Response;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Output\Output;

class TimeTracking
{
    /** @var \App\Services\ApiClient */
    private ApiClient $api;

    private ?Command $command;

    public static function secondsToTimeString($value)
    {
        $hours = floor($value / 3600);
        $minutes = floor(($value - ($hours * 3600)) / 60);
        $seconds = $value - ($hours * 3600) - ($minutes * 60);

        if ($hours < 10) {
            $hours = '0' . $hours;
        }
        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        if ($seconds < 10) {
            $seconds = '0' . $seconds;
        }
        return $hours . ':' . $minutes . ':' . $seconds;
    }

    public static function percentageForSeconds($value, $total)
    {
        if ($total <= 0) {
            return '-';
        }

        return number_format($value / $total * 100, 2, ',', '');
    }

    public function __construct(ApiClient $api)
    {
        $this->api = $api;
    }

    public function withToken(string $token): self
    {
        $this->api->withToken($token);

        return $this;
    }

    public function useCommand(Command $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function fetchStatus(string $date): Response
    {
        $this->command->comment('Requesting status of tracking', Output::VERBOSITY_VERBOSE);

        return tap($this->api->http()
            ->get('/api/time-trackings', [
                'date' => $date,
            ]), function (Response $response) {
            $this->command->comment('GET ' . $response->effectiveUri(), Output::VERBOSITY_VERBOSE);
        });
    }

    public function startTracking(string $type = 'work'): Response
    {
        $this->command->comment('Requesting start of tracking', Output::VERBOSITY_VERBOSE);

        return tap($this->api->http()
            ->post('/api/time-trackings', [
                'type' => $type,
            ]), function (Response $response) {
            $this->command->comment('POST ' . $response->effectiveUri(), Output::VERBOSITY_VERBOSE);
        });
    }

    public function stopTracking(): Response
    {
        $this->command->comment('Requesting stop of tracking', Output::VERBOSITY_VERBOSE);

        return tap($this->api->http()
            ->delete('/api/time-trackings'), function (Response $response) {
            $this->command->comment('DELETE ' . $response->effectiveUri(), Output::VERBOSITY_VERBOSE);
        });
    }

    public function verifyConfiguration()
    {
        try {
            $this->api->http();
        } catch (\RuntimeException $exception) {
            throw $exception;
        }
    }
}
