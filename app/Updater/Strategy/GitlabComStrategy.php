<?php

namespace App\Updater\Strategy;

use Humbug\SelfUpdate\Updater;
use LaravelZero\Framework\Components\Updater\Strategy\GitlabStrategy;
use LaravelZero\Framework\Components\Updater\Strategy\StrategyInterface;

class GitlabComStrategy extends GitlabStrategy implements StrategyInterface
{
    private $repoUrl = 'https://gitlab.com/mitarbeiterbereich/clients/mb-cli';

    public function download(Updater $updater)
    {
        file_put_contents(
            $updater->getTempPharFile(),
            file_get_contents($this->repoUrl . '/-/raw/master/builds/mb')
        );
    }

    public function getCurrentRemoteVersion(Updater $updater)
    {
        $config = file_get_contents($this->repoUrl . '/-/raw/master/config/app.php');

        if (!preg_match('~\'version\'\s?=>\s?\'(.*?)\'~', $config, $match)) {
            return '1000.0.0';
        }

        return $match[1];
    }

    public function getCurrentLocalVersion(Updater $updater)
    {
        return config('app.version');
    }
}
