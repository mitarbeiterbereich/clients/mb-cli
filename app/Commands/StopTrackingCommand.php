<?php

namespace App\Commands;

use App\Services\TimeTracking;
use Illuminate\Http\Client\RequestException;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Output\Output;

class StopTrackingCommand extends Command
{
    protected $signature = 'track:stop';
    protected $description = 'Stops tracking.';

    public function handle(TimeTracking $timeTracking)
    {
        $timeTracking->verifyConfiguration();

        $this->info('Stop tracking', Output::VERBOSITY_VERBOSE);

        try {
            $response = $timeTracking->useCommand($this)
                ->stopTracking();
            $this->info(sprintf('%d seconds logged for %s', $response->json('seconds_normalized'), $response->json('type')));
            $this->info('Stop tracking done', Output::VERBOSITY_VERBOSE);
            return 0;
        } catch (RequestException $exception) {
            if ($message = $exception->response->json('message')) {
                $this->error($message);
                return 1;
            }

            $this->error($exception->response->body());
            return 1;
        } catch (\RuntimeException $exception) {
            $this->error($exception->getMessage());
            return 1;
        }
    }
}
