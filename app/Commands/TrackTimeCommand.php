<?php

namespace App\Commands;

use App\Services\TimeTracking;
use Illuminate\Http\Client\RequestException;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Output\Output;

class TrackTimeCommand extends Command
{
    protected $signature = 'track:time
                            {--break : Track break instead of work time}';
    protected $description = 'Track work or break time begin.';

    public function handle(TimeTracking $timeTracking)
    {
        $timeTracking->verifyConfiguration();

        $type = $this->option('break') ? 'break' : 'work';
        $this->info('Start tracking ' . $type, Output::VERBOSITY_VERBOSE);

        try {
            $response = $timeTracking->useCommand($this)
                ->startTracking($type);
            $this->info(sprintf('Start tracking for %s', $response->json('type')));
            $this->info('Start tracking done', Output::VERBOSITY_VERBOSE);
            return 0;
        } catch (RequestException $exception) {
            if ($message = $exception->response->json('message')) {
                $this->error($message);
                return 1;
            }

            $this->error($exception->response->body());
            return 1;
        } catch (\RuntimeException $exception) {
            $this->error($exception->getMessage());
            return 1;
        }
    }
}
