<?php

namespace App\Commands;

use App\Services\TimeTracking;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Carbon;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Console\Output\Output;

class TrackStatusCommand extends Command
{
    protected $signature = 'track:status
                            {--date=today : Get status of date, date should be parsed like YYYY-MM-DD }';
    protected $description = 'Track status of a concrete date.';

    public function handle(TimeTracking $timeTracking)
    {
        $timeTracking->verifyConfiguration();

        try {
            $date = Carbon::parse($this->option('date'))->toDateString();
        } catch (InvalidFormatException $exception) {
            $this->error('Could not parse "' . $this->option('date') . '" as date');
            return 1;
        } catch (\RuntimeException $exception) {
            $this->error($exception->getMessage());
            return 1;
        }

        $this->info('Fetching tracking status', Output::VERBOSITY_VERBOSE);

        try {
            $response = $timeTracking->useCommand($this)
                ->fetchStatus($date);

            $this->output->table([
                'is_tracking' => ' ',
                'track_day' => 'Track Day',
                'work_time' => 'Work Time',
                'work_time_percentage' => '(%)',
                'work_time_planned' => 'Work Time Planned',
                'break_time' => 'Break Time',
                'break_time_percentage' => '(%)',
                'break_time_planned' => 'Break Time Planned',
            ], [
                [
                    $response->json('is_tracking') ? '🔴' : ' ',
                    $response->json('track_day'),
                    TimeTracking::secondsToTimeString($response->json('work_time')),
                    TimeTracking::percentageForSeconds($response->json('work_time'), $response->json('work_time_planned')),
                    TimeTracking::secondsToTimeString($response->json('work_time_planned')),
                    TimeTracking::secondsToTimeString($response->json('break_time')),
                    TimeTracking::percentageForSeconds($response->json('break_time'), $response->json('break_time_planned')),
                    TimeTracking::secondsToTimeString($response->json('break_time_planned')),
                ],
            ]);

            $this->info('Fetching tracking status done', Output::VERBOSITY_VERBOSE);
            return 0;
        } catch (RequestException $exception) {
            if ($message = $exception->response->json('message')) {
                $this->error($message);
                return 1;
            }

            $this->error($exception->response->body());
            return 1;
        }
    }
}
