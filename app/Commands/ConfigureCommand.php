<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

class ConfigureCommand extends Command
{
    protected $signature = 'configure';

    protected $description = 'Configures application';

    public function handle()
    {
        $apiToken = env('API_TOKEN');

        if ($apiToken) {
            if ($this->ask('Your application is already configured. Do you want to remove the current token?',
                    'y') === 'y') {
                $apiToken = null;
            }
        }

        if (!$apiToken) {
            $this->info('You can create an API Token at https://app.mitarbeiterbereich.de/user/api-tokens');
            $token = $this->secret('Which token do you have?');

            if ($token) {
                file_put_contents(
                    \Phar::running()
                        ? '.env'
                        : base_path('.env'),
                    'API_TOKEN="' . $token . '"'
                );

                $this->info('Your application is now configured.');
            }
        }
    }
}
