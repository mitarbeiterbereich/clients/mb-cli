<?php

namespace App\Providers;

use App\Services\ApiClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
    }

    public function register()
    {
        $this->app->bind(ApiClient::class, function () {
            return new ApiClient(
                env('BASE_URL', 'https://app.mitarbeiterbereich.de'),
                env('API_TOKEN')
            );
        });
    }
}
